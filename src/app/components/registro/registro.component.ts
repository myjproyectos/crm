import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit {

  formR: FormGroup

  constructor(public fb: FormBuilder, public authS: AuthService) { 
    this.formR = this.fb.group({
      name: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      correo: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
      pass: ['', [Validators.required, Validators.minLength(6)]]
    })
  }

  ngOnInit() {
    
  }

}
