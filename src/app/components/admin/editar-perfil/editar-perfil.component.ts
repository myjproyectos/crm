import { Component, OnInit } from '@angular/core';
import { CrudService } from "../../../services/crud.service";

@Component({
  selector: 'app-editar-perfil',
  templateUrl: './editar-perfil.component.html',
  styleUrls: ['./editar-perfil.component.scss']
})
export class EditarPerfilComponent implements OnInit {

  empresaI = ''
  nitI = '' 
  telefonoI = ''
  paisI = ''
  ciudadI = ''
  direccionI = ''

  perfil: any = ''

  constructor(public cs: CrudService) {}

  ngOnInit() {
    this.cs.getPerfil().subscribe((u)=>{
      this.perfil = u.payload.data()
    })
  }

}