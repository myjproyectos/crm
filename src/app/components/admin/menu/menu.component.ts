import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../../services/auth.service";
import { CrudService } from '../../../services/crud.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  foto: any = '';

  constructor(public authS: AuthService, private crud: CrudService) { }

  ngOnInit() {
   this.crud.getPerfil().subscribe((u) => {
     this.foto = u.payload.data()
   })
  }
}
