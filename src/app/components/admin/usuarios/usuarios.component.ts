import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CrudService } from 'src/app/services/crud.service';

declare var $: any

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {

  form: FormGroup

  areaT = ''
  areaInput = ''
  public areasN = []

  constructor(public cs: CrudService, public fb: FormBuilder) {
    this.form = this.fb.group({
      nombre: ['', [Validators.required]],
      apellido: ['', [Validators.required]],
      identificacion: [''],
      contacto: ['', [Validators.required]],
      email: ['', [Validators.required]],
      pass: ['', [Validators.required]],
      cargo: ['', [Validators.required]],
      area: ['', [Validators.required]]
    })
  }
//FIXME: Arreglar click bug cuando se elimina y cuando se quiere agregar un area
  ngOnInit() {
    this.cs.getAreas().subscribe((u) => {
      this.areasN = []
      u.forEach((d: any) => {
        this.areasN.push({
          id: d.payload.doc.id,
          data: d.payload.doc.data()
        })
      })
      setTimeout(() => {
        $('.hideDrop').on('click', function () {
          this.areaT = $(this).find('span').text()
          $('.showDrop option, .dropdown-trigger').val(this.areaT)
          $('.showDrop option').text(this.areaT)
          $('#dropdown1').css({ 'opacity': 0, 'display': 'none' })
        })
      }, 1000);
    })
    $('.tabs').tabs({
      swipeable: false,
    });
    $('select').formSelect();
    $('.dropdown-trigger').on('click', function () {
      $('#dropdown1').css({ 'opacity': 1, 'display': 'block' })
    })

    $(".addArea").on('click', function () {
      setTimeout(() => {
        $("#areaC").val('')
      }, 500);
    })
  }
}
