import { Component, OnInit } from '@angular/core';
import { CrudService } from 'src/app/services/crud.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare var $: any

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.scss']
})
export class VentasComponent implements OnInit {

  fecha = new Date()
  year: any

  ventas: any = ''
  formato: any = {
    Letras: '',
    Guion1: '',
    Guion2: ''
  }
  public lineas = []
  modalv = 0

  form: FormGroup
  formP: FormGroup

  constructor(public cs: CrudService, public fb: FormBuilder) {
    this.form = this.fb.group({
      linea: ['', [Validators.required]]
    })
  }

  ngOnInit() {
    this.year = this.fecha.getFullYear()
    this.cs.getPerfil().subscribe((u) => {
      this.ventas = u.payload.data()
    })
    this.cs.getPerfil2().subscribe((u) => {
      if (u.payload.data() != null) {
        this.formato = u.payload.data()
      }
    })
    this.cs.getLineas().subscribe((u) => {
      this.lineas = []
      u.forEach((d: any) => {
        this.lineas.push({
          id: d.payload.doc.id,
          data: d.payload.doc.data()
        })
      })
    })
    $('.collapsible').collapsible();
    $('.tabs').tabs({
      swipeable: false,
    });
  }

  limpiar() {
    setTimeout(() => {
      $("#linea").val('')
    }, 100);
  }

  modal(e) {
    this.modalv = e
  }
}
