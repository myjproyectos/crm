import { Injectable } from '@angular/core';
import { AuthService } from "../services/auth.service";
import { AngularFirestore } from "@angular/fire/firestore";
import { AngularFireStorage } from "@angular/fire/storage";
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  success = 0
  success2 = 0
  porcentaje
  menu = 3
  admin: any

  constructor(private af: AngularFirestore, public aS: AuthService, private storage: AngularFireStorage) {
    this.admin = this.af.collection('Administradores').doc(this.aS.user.uid)
  }

  async update(empresa: string, nit: string, telefono: string, pais: string, ciudad: string, direccion: string) {
    this.admin.update({
      Empresa: empresa,
      NIT: nit,
      Telefono: telefono,
      Pais: pais,
      Ciudad: ciudad,
      Direccion: direccion
    }).then(() => { this.success = 1 })

    setTimeout(() => {
      this.success = 0
    }, 3000)
  }
  //Obtengo los datos
  public getPerfil() {
    return this.admin.snapshotChanges()
  }

  public getPerfil2() {
    return this.admin.collection('Ventas').doc('Formato').snapshotChanges()
  }

  public getLineas() {
    return this.admin.collection('Ventas').doc('LineasNegocio').collection('Lineas').snapshotChanges()
  }
  
  public getAreas(){
    return this.admin.collection('Ventas').doc('AreasNegocio').collection('Areas').snapshotChanges()
  }
  //Agrego nuevos datos
  async lineas(data: string) {
    this.admin.collection('Ventas').doc('LineasNegocio').collection('Lineas').add({
      Linea: data,
      Pproducto: 0,
      Pservicio: 0
    })
  }

  async areas(area: string){
    this.admin.collection('Ventas').doc('AreasNegocio').collection('Areas').add({
      Area: area
    })
  }

  //Actualizo los datos
  async uploadImage(e) {
    var upload = this.storage.ref("Administradores/Logos/" + this.aS.user.uid).put(e.target.files[0])
    upload.then((url) => {
      url.ref.getDownloadURL().then((url) => {
        this.af.collection("Administradores").doc(this.aS.user.uid)
          .update({
            LogoEmpresa: url
          })
      })
      setTimeout(() => {
        this.porcentaje = null
      }, 2000)
    })
    this.porcentaje = upload.snapshotChanges().pipe(map(s => (s.bytesTransferred / s.totalBytes) * 100))
  }

  async consecutivos(letra: string, g1: string, num: string, g2: string, year: string, venta: string) {
    if (venta == '0') {
      this.admin.collection('Ventas').doc('Formato').set({
        Letras: letra,
        Guion1: g1,
        Numero: num,
        Guion2: g2,
        Year: year
      }).then(() => {
        this.admin.update({
          Ventas: '1'
        })
        this.success = 1
      })
    } else {
      this.admin.collection('Ventas').doc('Formato').update({
        Letras: letra,
        Guion1: g1,
        Numero: num,
        Guion2: g2,
        Year: year
      }).then(() => { this.success = 1 })
    }
    setTimeout(() => {
      this.success = 0
    }, 3000)
  }

  async updateL(p: any, s: any, id: any){
    this.admin.collection('Ventas').doc('LineasNegocio').collection('Lineas').doc(id).update({
      Pproducto: p,
      Pservicio: s
    }).then(() => { this.success2 = 1 })
    setTimeout(() => {
      this.success2 = 0
    }, 3000)
  }

  //Eliminar datos
  deleteL(linea: string) {
    this.admin.collection('Ventas').doc('LineasNegocio').collection('Lineas').doc(linea).delete()
  }
  deleteA(area: string){
    this.admin.collection('Ventas').doc('AreasNegocio').collection('Areas').doc(area).delete()
  }
  //Interaccion del menu
  async menuChange(e) {
    this.menu = e
  }

}
