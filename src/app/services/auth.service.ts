import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { AngularFireAuth } from "@angular/fire/auth";
import { User } from "firebase";
import * as firebase from "firebase/app";
import { AngularFirestore } from "@angular/fire/firestore";
import { Observable } from "rxjs";
import { take, map, tap } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: User;
  errorMessage = 0
  loader = 0
  errorLogin = 0

  constructor(public afAuth: AngularFireAuth, public router: Router, private af: AngularFirestore) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.user = user
        localStorage.setItem('user', JSON.stringify(this.user))
      } else {
        localStorage.setItem('user', null)
      }
    })
  }

  async login(email: string, password: string) {
    try {
      await this.afAuth.auth.signInWithEmailAndPassword(email, password)
      this.router.navigate(['/PerfilAdmin'])
    } catch (e) {
      if (e.name == 'Error') {
        this.errorLogin = 1
      }
    }
  }

  async loginGoogle() {
    const provider = new firebase.auth.GoogleAuthProvider()
    await this.afAuth.auth.signInWithPopup(provider)
      .then((r) => {
        this.loader = 1
        this.admin(r.user.email, r.user.displayName, '', r.user.photoURL)
      })
  }

  async loginTwitter() {
    const provider = new firebase.auth.TwitterAuthProvider()
    await this.afAuth.auth.signInWithPopup(provider)
      .then((r) => {
        this.loader = 1
        this.admin(r.user.email, r.user.displayName, '', r.user.photoURL)
      })
  }

  async loginFacebook() {
    const provider = new firebase.auth.FacebookAuthProvider()
    await this.afAuth.auth.signInWithPopup(provider)
      .then((r) => {
        this.loader = 1
        this.admin(r.user.email, r.user.displayName, '', r.user.photoURL)
      })
  }

  async logout() {
    await this.afAuth.auth.signOut()
    localStorage.removeItem('user')
    this.router.navigate(['/'])
    this.errorLogin = 0
    this.loader = 0
  }

  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'))
    return user != null
  }

  async register(email: string, password: string, name: string, lastName: string, telefono: string) {
    try {
      this.loader = 1
      await this.afAuth.auth.createUserWithEmailAndPassword(email, password)
        .then(() => {
          this.admin(email, name, lastName, '')
        })
    } catch (e) {
      if (e.code == 'auth/email-already-in-use') {
        this.errorMessage = 1
        this.loader = 0
      }
    }
  }

  async admin(email: string, name: string, lastName: string, photoURL: string) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.af.collection('Administradores').doc(this.user.uid).set({
          Nombre: name + ' ' + lastName,
          Correo: email,
          FotoPerfil: photoURL,
          Ventas: "0"
        }, {merge: true}).then(() => {
          this.router.navigate(['/PerfilAdmin'])
        })
      }
    })
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.afAuth.authState.pipe(take(1))
      .pipe(map(authState => !!authState)).pipe(tap(auth => {
        if (!auth) {
          this.router.navigate(['/'])
        }
      }))
  }
}
