import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegistroComponent } from './components/registro/registro.component';
import { PerfilComponent } from './components/admin/perfil/perfil.component';
import { AuthService } from "./services/auth.service";


const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'IniciarSesion', component: LoginComponent},
  {path: 'Registro', component: RegistroComponent},
  {path: 'PerfilAdmin', component: PerfilComponent, canActivate: [AuthService]},
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
