// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyClC8hd-6sFv1Ew9f2dEqjRtQT11BIBxuA",
    authDomain: "crm-myj.firebaseapp.com",
    databaseURL: "https://crm-myj.firebaseio.com",
    projectId: "crm-myj",
    storageBucket: "crm-myj.appspot.com",
    messagingSenderId: "675489110272",
    appId: "1:675489110272:web:59d622bd553bae81fc3be5",
    measurementId: "G-CYQ7W6KM4N"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
